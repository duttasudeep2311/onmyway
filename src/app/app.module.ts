import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './LoginComponent/login/login.component';
import { ForgetPasswordComponent } from './LoginComponent/forget-password/forget-password.component';
import { SignUpComponent } from './LoginComponent/sign-up/sign-up.component';
import { HomeComponent } from './HomeComponent/home/home.component';
import { HeaderComponent } from './Header&FooterComponent/header/header.component';
import { FooterComponent } from './Header&FooterComponent/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ForgetPasswordComponent,
    SignUpComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
