import { NgModule } from '@angular/core';
import { FormArray } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { FooterComponent } from './Header&FooterComponent/footer/footer.component';
import { HeaderComponent } from './Header&FooterComponent/header/header.component';
import { HomeComponent } from './HomeComponent/home/home.component';
import { ForgetPasswordComponent } from './LoginComponent/forget-password/forget-password.component';
import { LoginComponent } from './LoginComponent/login/login.component';
import { SignUpComponent } from './LoginComponent/sign-up/sign-up.component';

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "sign-up",
    component: SignUpComponent
  },
  {
    path: "forget-password",
    component: ForgetPasswordComponent
  },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "header",
    component: HeaderComponent
  },
  {
    path: "footer",
    component: FooterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
